import React from 'react';
import {Link} from "react-router-dom";

const CharacterCard = ({id, image, name, species, status, gender, location, origin, showLink,showLinkHome}) => {

    let linkButton = null;
    let charText = null;
    let style = null;

    if (showLink) {
        linkButton = <Link to={{pathname: '/character/' + id,}} className="btn btn-primary">View</Link>;
    }

    if (showLinkHome) {
        linkButton = <Link to={{pathname: '/',}} className="btn btn-primary">Home</Link>;
        charText = <h2>Your character is</h2>
        style = {
            margin: '0 40%'
        }

    }

    return (
        <div style={style} className="col-xs-12 col-sm-6 col-md-2 char-column">
            {charText}
            <div className="card character-card">
                <img src={image} className="card-img-top" alt={name}/>
                <div className="class-body">
                    <h5 className='card-title'>{name}</h5>
                    <b>Species: </b>{species}<br/>
                    <b>Status: </b>{status}<br/>
                    <b>Gender: </b>{gender}<br/>
                    <b>Location: </b>{location}<br/>
                    <b>Origin: </b>{origin}<br/>
                    {linkButton}
                </div>
            </div>
        </div>
    );
};

export default CharacterCard;