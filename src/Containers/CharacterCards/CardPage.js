import React from 'react';
import CharacterCard from "../../Components/CharacterCard";

class CardPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            character: null,
        };
    }

    componentDidMount() {
        this.getCharacterData();
    }

    getCharacterData = () => {
        fetch(`https://rickandmortyapi.com/api/character/${this.props.match.params.id}`)
            .then((resp) => resp.json())
            .then(data => {
                const recieved = data;
                if (recieved === undefined) {
                    throw new Error(data.error);
                } else {
                    this.setState({
                        character: recieved,
                    })
                }
            })
            .catch((error) => {
                console.error(error)
            })
    };

    render() {
        let characterCard = null;
        const state = this.state.character;
        console.log(state);
        if (state) {
            characterCard = (<CharacterCard key={state.id}
                                            id={state.id}
                                            image={state.image}
                                            name={state.name}
                                            species={state.species}
                                            status={state.status}
                                            gender={state.gender}
                                            location={state.location.name}
                                            origin={state.origin.name}
                                            showLinkHome={true}
            />)
        } else {
            characterCard =
                <h3> Loading...(If this takes too long, something probably went wrong ¯\_(ツ)_/¯, check the error logs!
                    )</h3>
        }
        return (
            <React.Fragment>{characterCard}</React.Fragment>
        )
    }
}

export default CardPage;