import React from 'react';
import CharacterCard from "../../Components/CharacterCard";


class CardCollection extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rickMorty: [],
            page: ''
        };
    }

    componentDidMount() {
        this.getData();
    }

    getData = () => {
        const min = Math.ceil(1);
        const max = Math.floor(25);
        const page = Math.floor(Math.random() * (max - min + 1)) + min;
        fetch(`https://rickandmortyapi.com/api/character/?page=${page}`)
            .then((resp) => resp.json())
            .then(data => {
                const recieved = data.results;
                if (recieved === undefined) {
                    throw new Error(data.error);
                } else {
                    this.setState({
                        rickMorty: recieved,
                        page: page
                    })
                }
            })
            .catch((error) => {
                console.error(error)
            })

    };

    filterCharacters = (collection, name) => {
        collection = this.state.rickMorty;
        const input = document.getElementById("search-input")
        name = input.value;
        const filtered = collection.filter(character => character.name.includes(name));
        if (filtered.length > 0){
            this.setState({
                rickMorty: filtered
            })
        } else {
            input.value = "";
            alert("No such character on this page!");
        }
    };

    resetCharacters = () => {
        const page = this.state.page;
        fetch(`https://rickandmortyapi.com/api/character/?page=${page}`)
            .then((resp) => resp.json())
            .then(data => {
                const recieved = data.results;
                if (recieved === undefined) {
                    throw new Error(data.error);
                } else {
                    this.setState({
                        rickMorty: recieved,
                        page: page
                    })
                }
            })
            .catch((error) => {
                console.error(error)
            })
    };

    render() {

        let characters = null;

        if (this.state.rickMorty.length > 0) {
            characters = this.state.rickMorty.map(character => (
                <CharacterCard key={character.id}
                               id={character.id}
                               image={character.image}
                               name={character.name}
                               species={character.species}
                               status={character.status}
                               gender={character.gender}
                               location={character.location.name}
                               origin={character.origin.name}
                               showLink={true}
                               currentPage={this.state.page}
                />
            ))
        } else {
            characters =
                <h3> Loading...(If this takes too long, something probably went wrong ¯\_(ツ)_/¯, check the error logs!
                    )</h3>
        }

        return (
            <React.Fragment>
                <h1>Characters from Richard and Mortimier</h1>
                <button className='btn-primary' onClick={this.getData}>Get new characters!</button>
                <button className='btn-primary' onClick={this.resetCharacters}>Reset characters</button>
                <div className='toolbar-div'>
                    <div className='searchbar-div'>
                        <div className="input-group mb-4">
                            <input id="search-input" type="search" placeholder="Filter characters"
                                   aria-describedby="button-addon5"
                                   className="form-control"/>
                            <div className="input-group-append">
                                <button onClick={this.filterCharacters} id="button-addon5" type="submit"
                                        className="btn btn-primary"><i
                                    className="fa fa-search">Search</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    {characters}
                </div>
            </React.Fragment>
        );
    }
}

export default CardCollection



